# Setting up a React application using Webpack4, Babel7, and ES6+
About a year ago I had some downtime from work and decided to take a look at the React library seeing as there was so much talk going on about it's benefits. But after reading some tutorials and articles I decided I was not the biggest fan of it and moved on. What put me off most about it was the way it mixed Javascript code along with HTML code. To me this just seemed like such a faux pas and was unpleasant to look at. 

But recently I decided to take another look at it. The difference this time around was that I saw how beneficial having Javascript and HTML mixed together in the same file would be. This allows for the logic and state data that a component relies on to be in the same file as the HTML code which will be using it. There would be no need to switch back and forth
between JavaScript files and HTML files. 

This style of code, that mixes HTML and JavaScript is actually it's own language, known as [JSX](https://reactjs.org/docs/introducing-jsx.html).
 
After deciding that I needed to give React another chance , I read through official docs and tutorials and was at a loss on how to integrate React into my preferred development ecosystem. Which included Webpack, Babel and ES6.

Currently there is a [simple command](https://www.npmjs.com/package/create-react-app) available in the NPM repository named `create-react-app` that allows you to quickly create a React app, but that method does not allow you to integrate it into your preferred development ecosystem. 

After a lot of trial and error, and a few hours lost going down the wrong path, I was finally able to integrate everything together. This experience is actually what made me write this article. 

So let's get started by going over the key components of the project we are going to create.

###### React
[React](https://reactjs.org/) is a JavaScript library that allows us to build UI components that are self-contained. Each component encapsulates it's own internal state. When used, components can be act more like objects, with each instance of a component being able to take input data, have it's on internal state data, and render the HTML output based
on it's state.

###### Webpack
[Webpack 4](https://github.com/webpack/webpack) as a JavaScript bundler. Webpack takes your Javascript code, as well as all of your code dependencies, and bundles all of it into a single file. 
 
Webpack is also extendable. It has a modular plugin system which allows us to plugin additional functionality. In the case of this tutorial we will be using plugins that will allow Webpack to also bundle our HTML and CSS 
dependencies.
 
###### Babel 7
ES6 has been around for a while now, but it is not fully supported across all browsers. Also, there are still users out there that are running older browser versions that do not support ES6. Since we want to take advantage of everything ES6+ (I will from this point on refer to ES6 and all subsequent ECMAScript releases that have to this date been released, this includes ES7 and ES8, as ES6+) has to offer we need to be able to convert our ES6+ code to plain JavaScript that all browsers can understand. Luckily for us there's [Babel](https://babeljs.io/).

Babel is a transpiler. What this means is that it can convert ES6+ and make it backwards compatible by translating ES6+ code into plain JavaScript.

Babel is also highly customizable. Out of the box it doesn't do any transformation on our code. But by adding plugins, or presets, which are pre-assembled plugin bundles, we can get it to transpile our ES6+ and React code.

## Prerequisites
If you don't already have Node.js v10 installed please go ahead and do so [here](https://nodejs.org/en/download/).

Node.js v10 is the preferred version in this tutorial due to [npx](https://www.npmjs.com/package/npx) being bundled with it. 

By reading the official package description we can see why `npx` is useful.

> `npx` executes \<command> either from a local node_modules/.bin, or from a central cache, installing any packages needed in order for <command> to run.By default, npx will check whether <command> exists in $PATH, or in the local project binaries, and execute that. If <command> is not found, it will be installed prior to execution.

This means that `npx` allows us to run Node binaries as if they were a shell command. This ability will prove helpful later.

## Project Setup
Lets start off by creating a project directory and making it our working directory.

```bash
mkdir FlaskWebpackBabelReact
cd FlaskWebpackBabelReact
```

#### Installation
Once we have created our new project directory we need to create the `package.json` file using `npm`, and install Webpack, Babel, along with all the plugins.

The commands below will create our `package.json` file and install the latest versions of Babel and the Babel plugins. As of the writing of this article this would be Babel 7.

```bash
npm init -y
npm install --save-dev @babel/core @babel/preset-env @babel/preset-react 
npm install --save-dev @babel/register
```

As the name implies, `@babel/core` is the core Babel library which is the self-contained Babel system. Liked mentioned before the Babel core library will not run any transformations. Which is why we also installed `@babel/preset-env` and `@babel/preset-react`. There are the presets, a group of Babel plugins,  that are going to transpile ES6+ and React in our JavaScript files. These presets will be used by Babel when it encounters code that would not traditionally be recognized, aka, ES6+ and React.

__Note: For clarity, `@babel/preset-env' allows us to use the latest JavaScript without specifying ES6, ES7, or ES8 specifically.__

`@babel/register` binds Babel's require hook to node's `require`. What this means is that all subsequent files required by Node will be transpiled by Babel before hand. This will be useful to us as you will see later in this
article when we run `webpack`.

__Note: I included `@babel/register` as a separate command from the other Babel packages because it's not a Babel 
plugin or preset.__

Next, we install the latest Webpack version, Webpack 4, and the necessary plugins.

```bash
npm install --save-dev webpack webpack-cli babel-loader css-loader
npm install --save-dev html-webpack-plugin mini-css-extract-plugin
```   

`webpack` and `webpack-cli` are the core and cli components of Webpack respectively. `webpack-cli` is necessary as part of `webpack` as of Webpack 4. 

`babel-loader` is a Webpack loader that allows it to preprocess files through Babel before Webpack bundles it. It works as the bridge between Webpack and Babel.

`css-loader` is similar to `babel-loader` in that it allows Webpack to load CSS files in JavaScript like this:

 `import './style.css'`
 
This plugin along with `mini-css-extract-plugin`  allow Webpack to bundle css assets.

`html-webpack-plugin` is a Webpack plugin that allows us to create a new html file that includes references to the newly created `.js` and `.css` bundles created by Webpack. This means that we don't have to manually change the references in our html files. This plugin takes care of that for us as part of each build.

Finally, we need to install React and React's DOM API library.

```bash
npm install --save-dev react react-dom
```

`react` is the core React library. While `react-dom` allows React to manipulate the DOM.

#### Configuration
It's now time to configure our build system.

First, let's create the .babelrc file needed by Babel. This file needs to be created in our projects root directory.

```bash
touch .babelrc
```    

We need to add the following lines to the file we just created.

```json
{
    "presets": [
      "@babel/preset-env",
      "@babel/preset-react"
    ]
}
```

As was mentioned earlier, we need to let Babel know what plugins, or presets, it needs to use when parsing our JavaScript files. Here we are telling it to use the presets for ES6+ and React. 

Next, we need to create the `webpack.config.js` file. This is the file where we will configure our build process. This file tells `webpack` what plugins and libraries it needs, as well as telling it what steps it needs to take as part of the bundling process.

```bash
touch webpack.config.js
```

Edit the `webpack.config.js` file to contain the following code.

```javascript
import path from 'path';
import HtmlWebpackPlugin from'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export default {
    entry: {
        bundle: path.join(__dirname, 'src/index.js'),
        vendor: ['react', 'react-dom']
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            },

            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'src/index.html'),
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ]
};
```

I will not go into too much detail on the code above. For a deeper understanding of Webpack configuration you can read up on it [here](https://webpack.js.org/concepts/).

I will point out some key pieces of the code though.

```javascript
entry: {
        bundle: path.join(__dirname, 'src/index.js'),
        vendor: ['react', 'react-dom']
    },
```

`entry.vendor` is an array listing the external libraries that are being used in this project. This tells `webpack` to bundle and generate the bundled `.js` file for these third-party vendor libraries separately from our code.

```javascript
rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            },

            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            }
        ]
```

In the rules section of the configuration we setup two rules. One handles `.js` files and the other `.css` files.

`test: /\.js$/` is a regular expression telling it to look for files ending in `.js`. We also tell it to exclude any files that are in the `node_modules` folder from this rule. The line `loader: 'babel-loader'` tells Webpack what plugin to use along with this rule. In this case we are telling it to pre-process `.js` files through the `babel-loader` which will do the JavaScript transformation before `webpack` bundles it.

The rule for `.css` files is similar to the one for `.js` files. But in this case we are using two loaders `[MiniCssExtractPlugin.loader, 'css-loader]`. We need to pass it in that specific order since Webpack chains loaders right to left. So the `css-loader` gets called first. It then passes it's output to `MiniCssExtractPlugin.loader`. This entry handles import calls of `.css` files, as well as how they are bundled.

```javascript
plugins: [
    new HtmlWebpackPlugin({
        template: path.join(__dirname, 'src/index.html'),
    }),
    new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id].css'
    })
]
```

This section tells `webpack` where to find the template `index.html` file, which we will create later on, that will be reference our bundled `.js` and `.css` files. 

That's followed by a section that sets the naming rules for bundled `.css` file that will be generated.

The final thing we need to do as part of the configuration is add our build command to the `scripts` section of the `package.json` file.

```json
"scripts": {
  "build": "webpack --config-register @babel/register"
}
```

This creates an alias name for a script which runs the `webpack` binary. As part of the call to `webpack` we set the option `--config-register @bable/register`. This tells `webpack` to use `@babel/register` when reading the `webpack.config.js` file itself. If we did not add this option then `webpack` would not be able to read the config file since it contains ES6+ code, which `webpack` can not read. By including the option we tell it to run the config through Babel first and transpile it for consumption by `webpack`.

__Note: when we call `webpack` from our script above we are taking advantage of `npx`. As mentioned earlier `npx`
allows node to call local binaries.__

#### Application Code
Next we create the files where our actual code will reside. I will not go into any detail with this code since it's all straightforward. I will say that if you take a look at the `index.js` file you will notice that it contains ES6 and React code.

```bash
mkdir src
touch src/index.html src/index.js src/index.css
```

Add the following code for a simple React App to the `src/index.js`:

```javascript
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css'

class App extends Component {
    render() {
        return (
            <div className="App">
                <h1 className="main-header">Hello There!</h1>
            </div>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById('root'));
```

Add the following HTML code to `src/index.html`:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <title>FlaskWebpackBabelReact</title>
</head>
<body>
    <div id="root"></div>
</body>
</html>
```

Add the following CSS styling to `src/index.css`:

```css
.main-header{
    color: aquamarine;
}
```

We now have all the configuration and code necessary to build and run our simple application. We can now run the build and package it all together by running the command:

```bash
npm run build
```

This runs the script that we added in `package.json` which kicks-off `webpack`.

You should now have a folder named `dist` in the root directory, which should have the files that were created by `webpack`. These include `bundle.bundle.js`, `vendor.bundle.js`, and `index.html`.

If you take a look inside `index.html` you'll see that it contains `<script>` tags referencing the `.bundle.js` files. And a <link> tag referencing the `.bundle.css` file.

To view the page you can either load `dist/index.html` in to the browser, or you can use your web server of choice.

![Image](public/final.png)

That's it for this tutorial. Thanks for following along.

[GitHub](https://github.com/albertacuna/FlaskWebpackBabelReact)